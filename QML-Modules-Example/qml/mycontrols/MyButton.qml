import QtQuick 2.0

Rectangle {
    id: root
    color: "red"
    radius: 20
    property bool toggled: false
    function updateColor() {
        root.color = toggled ? "green" : "red"
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            root.toggled = !root.toggled
            updateColor()
        }
    }
    Text {
        text: qsTr("Button From MyControls Module \n Click Me")
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        wrapMode: Text.WordWrap
        font.bold: true
    }
}
