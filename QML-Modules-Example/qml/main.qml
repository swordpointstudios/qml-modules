import QtQuick 2.11
import QtQuick.Controls 1.4
import mycontrols 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Tabs")
    ExampleButton {
        width: parent.width / 2
        height: parent.height / 2
        anchors.centerIn: parent
    }
}
